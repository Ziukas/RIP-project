class Network {

    var routersList: ArrayList<Router> = ArrayList()

    fun addRouter(id: String?) {
        for (router in routersList) {
            if (router.routerId == id) {
                println("Router $id already exist")
                return
            }
        }
        routersList.add(Router(id))
        println("Router $id added")

    }

    fun removeRouterById(id: String) {
        var neighbourToRemove: Neighbour? = null
        var routerToRemove: Router? = null
        var pathToRemove: ArrayList<Path>? = ArrayList()

        routersList.remove(routersList.find { router -> router.routerId == id })

        for (router in routersList) {
            if (router.hasNeighbour(id)) {
                router.removeNeighbour(id)
            }
        }

//        for (router in routersList) {
//            if (router.routerId == id) {
//                routerToRemove = router
//            }
//
//            for (neighbour in router.neighboursList) {
//                if (neighbour.router?.routerId == id) {
//                    neighbourToRemove = neighbour
//                }
//            }
//            for (path in router.pathsList){
//                if(path.nextHop == id){
//                    pathToRemove?.add(path)
//                }
//            }
//            router.neighboursList.remove(neighbourToRemove)
//            for (path in pathToRemove!!){
//                router.pathsList.remove(path)
//            }
//        }
//        routersList.remove(routerToRemove)
    }

    private fun getListOfNeighbours(router: Router): ArrayList<Neighbour> {
        return router.neighboursList
    }

    private fun findRouter(id: String?): Router? {
        for (router in routersList) {
            if (router.routerId == id) {
                return router
            }
        }
        return null
    }

    fun getRouterListSize() = routersList.size

    fun addLink(firstRouterId: String?, secondRouterId: String?, cost: String?) {
        val router1 = findRouter(firstRouterId)
        val router2 = findRouter(secondRouterId)

        router1!!.addLink(Neighbour(router2, cost?.toInt()))
        router2!!.addLink(Neighbour(router1, cost?.toInt()))
    }

    fun checkForNeighbour(routerId: String) {
        val router1 = findRouter("1")
        println(router1!!.hasNeighbours(routerId)?.router?.routerId)
    }

    fun showAllNeighbours() {
        for (router in routersList) {
            println("Router ${router.routerId} table: ")
            for (neighbour in router.neighboursList) {
                println("link to ${neighbour.router?.routerId}, cost: ${neighbour.cost}")
            }
            for (path in router.pathsList) {
                println("path to: ${path.destination} with next hop to ${path.nextHop}, cost ${path.cost} and number of hops ${path.hops}")
            }

        }
    }

    fun showRoutingTableAndNeighbours(routerId: String?) {
        for (path in routersList.find { router -> router.routerId == routerId }!!.pathsList) {
            println("path to: ${path.destination} with next hop to ${path.nextHop}, cost ${path.cost} and number of hops ${path.hops}")
        }

    }

    fun displayAllRouters() {
        println("All routers in a network: ")
        for (router in routersList) {
            println(router.routerId)
        }
    }

    fun addNewLink() {
        for (router in routersList) {
            for (neighbour in router.neighboursList) {
                connectLinks(router.routerId, neighbour.router?.routerId)
            }

        }
    }

    private fun connectLinks(sourceRouterId: String?, destinationRouterId: String?) {
        val sourceRouter = findRouter(sourceRouterId)
        val destinationRouter = findRouter(destinationRouterId)

        for (path in sourceRouter!!.pathsList) {
            destinationRouter?.findPath(path, sourceRouterId, true)
        }
    }

    fun sendMessage(firstRouter: String?, secondRouter: String?, message: String?) {
        println("message in $firstRouter")
        if (firstRouter != secondRouter) {
            for (router in routersList) {
                if (router.routerId == firstRouter) {
                    for (path in router.pathsList) {
                        if (path.destination == secondRouter && path.cost != -1) {
                            sendMessage(path.nextHop, secondRouter, message)
                        }
                    }
                }
            }
        } else {
            println("Message '$message' delivered to router $firstRouter")
        }
    }

}