interface MenuOptionListener {

    fun onAddRouterSelected(routerId: String?)

    fun onRemoveRouterSelected(routerId: String?)

    fun onAddLinkSelected(firstRouter: String?, secondRouter: String?, cost: String?)

    fun onRemoveLinkSelected(firstRouter: String?, secondRouter: String?)

    fun onSendMessageSelected(sourceRouter: String?, destinationRouter: String?, message: String?)

    fun onShowAllTablesSelected()

    fun onShowTableSelected(routerId: String?)

}