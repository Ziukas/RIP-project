object Presets {


    enum class PresetType {
        LINE, CIRCLE, COMPLEX_1, COMPLEX_2
    }

    fun getSelectedPreset(presetType: PresetType): Preset?{
        return when(presetType){
            PresetType.LINE -> linearPreset()
            PresetType.CIRCLE -> circlePreset()
            PresetType.COMPLEX_1 -> complex1Preset()
            PresetType.COMPLEX_2 -> complex2Preset()
        }
        return null
    }


    // 1 - 2 - 3
    private fun linearPreset(): Preset{

        var routers: ArrayList<Int> = ArrayList()
        val links: ArrayList<Links> = ArrayList()

        routers.add(1)
        routers.add(2)
        routers.add(3)

        links.add(Links(1, 2, 12))
        links.add(Links(2, 3, 23))

        return Preset(routers, links)
    }


    // 1 - 3
    // |   |
    // 2 - 4
    private fun circlePreset(): Preset {

        var routers: ArrayList<Int> = ArrayList()
        val links: ArrayList<Links> = ArrayList()

        routers.add(1)
        routers.add(2)
        routers.add(3)
        routers.add(4)

        links.add(Links(1, 2, 12))
        links.add(Links(1, 3, 1))
        links.add(Links(2, 4, 24))
        links.add(Links(3, 4, 34))

        return Preset(routers, links)
    }

    private fun complex1Preset(): Preset? {
        var routers: ArrayList<Int> = ArrayList()
        val links: ArrayList<Links> = ArrayList()

        routers.add(1)
        routers.add(2)
        routers.add(3)
        routers.add(4)
        routers.add(5)
        routers.add(6)
        routers.add(7)
        routers.add(8)
        routers.add(9)
        routers.add(10)

        links.add(Links(1, 2, 12))
        links.add(Links(1, 4, 1000))
        links.add(Links(1, 10, 1000))
        links.add(Links(2, 3, 2000))
        links.add(Links(2, 7, 27))
        links.add(Links(4, 9, 2))
        links.add(Links(9, 5, 3))
        links.add(Links(5, 3, 1))
        links.add(Links(7, 8, 78))
        links.add(Links(8, 6, 68))
        links.add(Links(6, 10, 60))

        return Preset(routers, links)
    }

    private fun complex2Preset(): Preset? {
        var routers: ArrayList<Int> = ArrayList()
        val links: ArrayList<Links> = ArrayList()

        routers.add(1)
        routers.add(2)
        routers.add(3)
        routers.add(4)
        routers.add(5)
        routers.add(6)

        links.add(Links(1, 2, 1))
        links.add(Links(1, 3, 100))
        links.add(Links(2, 3, 200))
        links.add(Links(2, 5, 2))
        links.add(Links(2, 4, 300))
        links.add(Links(5, 6, 3))
        links.add(Links(4, 6, 4))
        links.add(Links(4, 3, 5))

        return Preset(routers, links)
    }



}