import javafx.application.Application.launch

object Main : MenuOptionListener {

    lateinit var network: Network

    @JvmStatic
    fun main(args: Array<String>) {
        network = Network()

        runPresets(Presets.PresetType.COMPLEX_1)

        Menu(this)
    }

    override fun onAddRouterSelected(routerId: String?) {
        network.addRouter(routerId)
    }

    override fun onRemoveRouterSelected(routerId: String?) {
        network.removeRouterById(routerId.toString())
    }

    override fun onAddLinkSelected(firstRouter: String?, secondRouter: String?, cost: String?) {
        network.addLink(firstRouter, secondRouter, cost)
    }

    override fun onRemoveLinkSelected(firstRouter: String?, secondRouter: String?) {
        network.addNewLink()
    }

    override fun onShowTableSelected(routerId: String?) {
        iteratePaths()
        network.showRoutingTableAndNeighbours(routerId)
    }

    override fun onSendMessageSelected(sourceRouter: String?, destinationRouter: String?, message: String?) {
        iteratePaths()
        network.sendMessage(sourceRouter, destinationRouter, message)
    }

    override fun onShowAllTablesSelected() {
        iteratePaths()
        network.showAllNeighbours()
    }

    private fun iteratePaths() {
        for (i in 1.. network.getRouterListSize()){
            network.addNewLink()
        }
    }

    private fun runPresets(presetType: Presets.PresetType){
        val preset =  Presets.getSelectedPreset(presetType)

        for (router in preset!!.routers){
            network.addRouter(router.toString())
        }
        for (link in preset.links){
            network.addLink(link.r1.toString(), link.r2.toString(), link.cost.toString())
        }
    }
}