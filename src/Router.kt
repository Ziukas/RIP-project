class Router(val routerId: String?) {

    val neighboursList: ArrayList<Neighbour> = ArrayList()
    val pathsList: ArrayList<Path> = ArrayList()

    fun addLink(neighbour: Neighbour) {
        neighboursList.add(neighbour)
        findPath(Path(neighbour.router?.routerId, neighbour.router?.routerId, neighbour.cost, 1), neighbour.router?.routerId, false)
        println("link ${neighbour.router!!.routerId} added to ${this.routerId}")
    }

    fun findPath(path: Path, from: String?, calculateCost: Boolean) {

        if (path.destination == this.routerId || path.nextHop == this.routerId) return

        var newCost: Int = when {
            calculateCost -> path.cost!!.toInt() + hasNeighbours(from)?.cost!!
            else -> path.cost!!.toInt()
        }

        if (path.cost == -1){
            newCost = -1
        }

        val oldPath = isPathAlreadyExist(path.destination)

        if (oldPath == null) {
            if (calculateCost) {
                val hops = path.hops!! + 1
                pathsList.add(Path(path.destination, from, newCost, hops))
            } else {
                pathsList.add(Path(path.destination, from, newCost, path.hops))
            }
        } else if (newCost == -1 && from == oldPath.destination) {
            pathsList.remove(oldPath)
            if(hasNeighbour(path.destination!!)){
                val neighbour = neighboursList.find { neighbour -> neighbour.router?.routerId == path.destination }
                pathsList.add(Path(neighbour?.router?.routerId, from, neighbour?.cost, 1))
            } else {
                pathsList.remove(oldPath)
                pathsList.add(Path(path.destination, from, newCost, -1))
            }
        } else if (oldPath.nextHop== from || oldPath.cost == -1 || (newCost < oldPath.cost!!.toInt() && newCost != -1)) {
            pathsList.remove(oldPath)
            val hops = path.hops!! + 1

            pathsList.add(Path(path.destination, from, newCost, hops))
        }
    }

    fun hasNeighbours(routerId: String?): Neighbour? {
        for (neighbour in neighboursList) {
            if (neighbour.router?.routerId == routerId) {
                return neighbour
            }
        }
        return null
    }

    fun isPathAlreadyExist(routerId: String?): Path? {
        for (path in pathsList) {
            if (path.destination == routerId) {
                return path
            }
        }
        return null
    }

    fun removeNeighbour(id: String) {

        neighboursList.remove(neighboursList.find { neighbour -> neighbour.router?.routerId == id })

        var p = pathsList.find { path -> path.destination == id && path.nextHop == id }

        if (p != null) {
            p.cost = -1
        }

        for (pathId in 0 until pathsList.count()) {
            if (pathsList[pathId].nextHop == id) {
                val destinationPath = pathsList[pathId].destination
                if (!hasNeighbour(destinationPath!!)) {
                    pathsList[pathId].cost = -1
                } else {
                    pathsList.remove(pathsList[pathId])
                    val neighbour = neighboursList.find { neighbour -> neighbour.router?.routerId == destinationPath }
                    findPath(Path(neighbour?.router?.routerId, neighbour?.router?.routerId, neighbour?.cost, 1), neighbour?.router?.routerId, false)
                }


            }
        }

    }

    fun hasNeighbour(id: String): Boolean {
        for (neighbour in neighboursList) {
            if (neighbour.router?.routerId == id) {
                return true
            }
        }
        return false
    }
}