import java.text.ParseException
import kotlin.math.cos

class Menu(private val menuOptionListener: MenuOptionListener) {

    init {
        loadMenu()
    }

    private fun loadMenu() {
        println("WELCOME")
        println("Add a router - 1")
        println("Remove a router - 2")
        println("Add link - 3")
        println("Remove link - 4")
        println("Send message - 5")
        println("Display table for one router - 6")
        println("show all routers in a network - 7")
        println("Exit - 0")
        handleSelectedOption(readLine()?.toInt())

    }

    private fun handleSelectedOption(input: Int?) {
        while (true) {
            when (input) {
                1 -> addRouter()
                2 -> removeRouter()
                3 -> addLink()
                4 -> removeLink()
                5 -> sendMessage()
                6 -> showRoutingTable()
                7 -> showAllTables()
                else -> println("Wrong input")
            }
            loadMenu()
        }
    }

    private fun showAllTables() {
        menuOptionListener.onShowAllTablesSelected()
    }

    private fun addRouter() {
        println("Enter router id: ")
        menuOptionListener.onAddRouterSelected(getInput(readLine()))
    }

    private fun removeRouter(){
        println("Enter router id: ")
        menuOptionListener.onRemoveRouterSelected(readLine())
    }

    private fun addLink(){
        println("Enter first router id: ")
        val firstRouter = readLine()
        println("Enter second router id: ")
        val secondRouter = readLine()
        println("Enter cost: ")
        val cost = readLine()
        try {
            menuOptionListener.onAddLinkSelected(firstRouter, secondRouter, cost)
        } catch (e: Exception){
            println("Not a number")
        }
    }

    private fun removeLink(){
        println("Enter first router id: ")
        val firstRouter = readLine()
        println("Enter second router id: ")
        val secondRouter = readLine()
        println("Enter cost: ")
        val cost = readLine()
        menuOptionListener.onRemoveLinkSelected(firstRouter, secondRouter)
    }

    private fun sendMessage() {
        println("Enter first router id: ")
        val firstRouter = readLine()
        println("Enter second router id: ")
        val secondRouter = readLine()
        println("Enter message: ")
        val message = readLine()
        menuOptionListener.onSendMessageSelected(firstRouter, secondRouter, message)
    }

    private fun showRoutingTable() {
        menuOptionListener.onShowTableSelected(readLine())
    }

    fun getInput(input: String?): String? {
        if (input.isNullOrEmpty()){
            println("WTF")
            return "WTF"
        }
        else return input
    }


}